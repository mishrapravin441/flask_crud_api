#from .Utils import DB, login
from flask_crud_apis_Fix import DB,login
from .search import add_to_index, remove_from_index, query_index
from flask_login import UserMixin


class SearchableMixin(object):
    @classmethod
    def search(cls, expression, page):
        ids, total = query_index(cls.__tablename__, expression, page, cls.__searchable__)
        if total == 0:
            return cls.query.filter_by(id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return cls.query.filter(cls.id.in_(ids)), total

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


DB.event.listen(DB.session, 'before_commit', SearchableMixin.before_commit)
DB.event.listen(DB.session, 'after_commit', SearchableMixin.after_commit)


class User(UserMixin, DB.Model):
    """User database"""
    id = DB.Column(DB.Integer, primary_key=True)
    public_id = DB.Column(DB.String(50), unique=True)
    name = DB.Column(DB.String(50))
    password = DB.Column(DB.String(80))
    admin = DB.Column(DB.Boolean)

    @login.user_loader
    def load_user(id):
        return User.query.get(int(id))

    def __repr__(self):
        return '{self.name}', '{self.public_id}', '{self.id}', '{self.last_seen}'


class Questions(SearchableMixin, DB.Model):
    """Question database"""
    id = DB.Column(DB.Integer, primary_key=True)
    text = DB.Column(DB.Text())
    complete = DB.Column(DB.Boolean)
    user_id = DB.Column(DB.Integer)
    __searchable__ = ['text']

    def __repr__(self):
        return f"('{self.id}', '{self.text})"


class AnswersDatas(SearchableMixin, DB.Model):
    """Answer database"""
    id = DB.Column(DB.Integer, primary_key=True)
    solution = DB.Column(DB.Text())
    complete = DB.Column(DB.Boolean)
    question_id = DB.Column(DB.Integer)
    user_id = DB.Column(DB.Integer)
    __searchable__ = ['solution']

    def __repr__(self):
        return f"('{self.id}', '{self.solution}')"
