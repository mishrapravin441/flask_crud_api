from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from .models import User
from wtforms import IntegerField, TextAreaField
from flask_babel import _, lazy_gettext as _l
from flask import request
from flask_login import current_user
from werkzeug.security import check_password_hash

class SearchForm(FlaskForm):
    q = StringField(_l('Search'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(name=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')


class ChangePasswordForm(FlaskForm):
    oldpassword = PasswordField('Old Password', validators=[DataRequired()])
    newpassword= PasswordField('New Password', validators=[DataRequired()])
    confirmnewpassword = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('newpassword')])
    submit = SubmitField('Change Password')

    def validate_oldpassword(self, oldpassword):
        if not check_password_hash(current_user.password, oldpassword.data) :
            raise ValidationError('Invalid Password.')
        if oldpassword.data == None:
            raise ValidationError('Please enter password')

class AddQuestionForm(FlaskForm):
    question = TextAreaField('Question', validators=[DataRequired()])
    submit = SubmitField('Submit')


class WriteAnswerForm(FlaskForm):
    answer = TextAreaField('Answer', validators=[DataRequired()])
    submit = SubmitField('Submit')



