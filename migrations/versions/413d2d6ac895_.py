"""empty message

Revision ID: 413d2d6ac895
Revises: 496e3f29a5ce
Create Date: 2020-03-30 17:48:51.505065

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '413d2d6ac895'
down_revision = '496e3f29a5ce'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('answers_data')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('answers_data',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('solution', sa.VARCHAR(length=50), nullable=True),
    sa.Column('complete', sa.BOOLEAN(), nullable=True),
    sa.Column('question_id', sa.INTEGER(), nullable=True),
    sa.CheckConstraint('complete IN (0, 1)'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###
